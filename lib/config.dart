class ConfigTypes {
  static const String PROD = 'prod';
}

class ConfigEnvironments {
  static const String _currentConfig = ConfigTypes.PROD;
  static const List<Map<String, String>> _availableEnvironments = [
    {
      'env': ConfigTypes.PROD,
      'url': 'https://api.github.com/',
    },
  ];

  static Map<String, String> getEnvironments() {
    return _availableEnvironments.firstWhere((d) => d['env'] == _currentConfig);
  }
}