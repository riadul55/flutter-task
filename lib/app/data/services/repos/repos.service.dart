import 'dart:convert';

import 'package:get/get.dart';

import 'dto/repos.response.dart';

class ReposService {
  final GetConnect _connect;

  const ReposService(GetConnect connect) : _connect = connect;

  // String get _prefix => 'api';

  Future<ReposResponse> getRepositories({page, sort}) async {
    return await _connect.get("search/repositories", headers: {
      "Accept": "application/vnd.github+json"
    }, contentType: "application/json", query: {
      "q": "Flutter",
      "page": "$page",
      "per_page": "10",
      "sort": "$sort",
      "order": "desc",
    }).then((response) {
            print(jsonDecode(response.body));
            return response.statusCode == 200
                ? ReposResponse.fromJson(response.body)
                : throw (response.body);
          },
    );
  }
}