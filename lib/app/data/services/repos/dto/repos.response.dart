class ReposResponse {
/*
{
  "total_count": 410108,
  "incomplete_results": false,
  "items": [
    {
      "id": 31792824,
      "full_name": "flutter/flutter",
      "private": false,
      "owner": {
        "avatar_url": "https://avatars.githubusercontent.com/u/14101776?v=4",
        "type": "Organization"
      },
      "html_url": "https://github.com/flutter/flutter",
      "description": "Flutter makes it easy and fast to build beautiful apps for mobile and beyond",
      "created_at": "2015-03-06T22:54:58Z",
      "updated_at": "2022-08-13T09:55:35Z",
      "pushed_at": "2022-08-13T09:49:01Z",
      "stargazers_count": 143530,
      "watchers_count": 143530,
      "language": "Dart",
      "forks_count": 22935,
      "open_issues_count": 11014,
      "license": {
        "key": "bsd-3-clause",
        "name": "BSD 3-Clause \"New\" or \"Revised\" License",
        "spdx_id": "BSD-3-Clause",
        "url": "https://api.github.com/licenses/bsd-3-clause"
      },
      "topics": [
        "android"
      ],
      "visibility": "public",
      "default_branch": "master",
      "score": 1
    }
  ]
}
*/

  int? totalCount;
  bool? incompleteResults;
  List<ReposResponseItems>? items;

  ReposResponse({
    this.totalCount,
    this.incompleteResults,
    this.items,
  });
  ReposResponse.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count']?.toInt();
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      final v = json['items'];
      final arr0 = <ReposResponseItems>[];
      v.forEach((v) {
        arr0.add(ReposResponseItems.fromJson(v));
      });
      items = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total_count'] = totalCount;
    data['incomplete_results'] = incompleteResults;
    if (items != null) {
      final v = items;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['items'] = arr0;
    }
    return data;
  }
}

class ReposResponseItemsLicense {
/*
{
  "key": "bsd-3-clause",
  "name": "BSD 3-Clause \"New\" or \"Revised\" License",
  "spdx_id": "BSD-3-Clause",
  "url": "https://api.github.com/licenses/bsd-3-clause"
}
*/

  String? key;
  String? name;
  String? spdxId;
  String? url;

  ReposResponseItemsLicense({
    this.key,
    this.name,
    this.spdxId,
    this.url,
  });
  ReposResponseItemsLicense.fromJson(Map<String, dynamic> json) {
    key = json['key']?.toString();
    name = json['name']?.toString();
    spdxId = json['spdx_id']?.toString();
    url = json['url']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['key'] = key;
    data['name'] = name;
    data['spdx_id'] = spdxId;
    data['url'] = url;
    return data;
  }
}

class ReposResponseItemsOwner {
/*
{
  "avatar_url": "https://avatars.githubusercontent.com/u/14101776?v=4",
  "type": "Organization"
}
*/

  String? avatarUrl;
  String? type;

  ReposResponseItemsOwner({
    this.avatarUrl,
    this.type,
  });
  ReposResponseItemsOwner.fromJson(Map<String, dynamic> json) {
    avatarUrl = json['avatar_url']?.toString();
    type = json['type']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['avatar_url'] = avatarUrl;
    data['type'] = type;
    return data;
  }
}

class ReposResponseItems {
/*
{
  "id": 31792824,
  "full_name": "flutter/flutter",
  "private": false,
  "owner": {
    "avatar_url": "https://avatars.githubusercontent.com/u/14101776?v=4",
    "type": "Organization"
  },
  "html_url": "https://github.com/flutter/flutter",
  "description": "Flutter makes it easy and fast to build beautiful apps for mobile and beyond",
  "created_at": "2015-03-06T22:54:58Z",
  "updated_at": "2022-08-13T09:55:35Z",
  "pushed_at": "2022-08-13T09:49:01Z",
  "stargazers_count": 143530,
  "watchers_count": 143530,
  "language": "Dart",
  "forks_count": 22935,
  "open_issues_count": 11014,
  "license": {
    "key": "bsd-3-clause",
    "name": "BSD 3-Clause \"New\" or \"Revised\" License",
    "spdx_id": "BSD-3-Clause",
    "url": "https://api.github.com/licenses/bsd-3-clause"
  },
  "topics": [
    "android"
  ],
  "visibility": "public",
  "default_branch": "master",
  "score": 1
}
*/

  int? id;
  String? fullName;
  bool? private;
  ReposResponseItemsOwner? owner;
  String? htmlUrl;
  String? description;
  String? createdAt;
  String? updatedAt;
  String? pushedAt;
  int? stargazersCount;
  int? watchersCount;
  String? language;
  int? forksCount;
  int? openIssuesCount;
  ReposResponseItemsLicense? license;
  List<String?>? topics;
  String? visibility;
  String? defaultBranch;
  int? score;

  ReposResponseItems({
    this.id,
    this.fullName,
    this.private,
    this.owner,
    this.htmlUrl,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.pushedAt,
    this.stargazersCount,
    this.watchersCount,
    this.language,
    this.forksCount,
    this.openIssuesCount,
    this.license,
    this.topics,
    this.visibility,
    this.defaultBranch,
    this.score,
  });
  ReposResponseItems.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    fullName = json['full_name']?.toString();
    private = json['private'];
    owner = (json['owner'] != null) ? ReposResponseItemsOwner.fromJson(json['owner']) : null;
    htmlUrl = json['html_url']?.toString();
    description = json['description']?.toString();
    createdAt = json['created_at']?.toString();
    updatedAt = json['updated_at']?.toString();
    pushedAt = json['pushed_at']?.toString();
    stargazersCount = json['stargazers_count']?.toInt();
    watchersCount = json['watchers_count']?.toInt();
    language = json['language']?.toString();
    forksCount = json['forks_count']?.toInt();
    openIssuesCount = json['open_issues_count']?.toInt();
    license = (json['license'] != null) ? ReposResponseItemsLicense.fromJson(json['license']) : null;
    if (json['topics'] != null) {
      final v = json['topics'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      topics = arr0;
    }
    visibility = json['visibility']?.toString();
    defaultBranch = json['default_branch']?.toString();
    score = json['score']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['full_name'] = fullName;
    data['private'] = private;
    if (owner != null) {
      data['owner'] = owner!.toJson();
    }
    data['html_url'] = htmlUrl;
    data['description'] = description;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['pushed_at'] = pushedAt;
    data['stargazers_count'] = stargazersCount;
    data['watchers_count'] = watchersCount;
    data['language'] = language;
    data['forks_count'] = forksCount;
    data['open_issues_count'] = openIssuesCount;
    if (license != null) {
      data['license'] = license!.toJson();
    }
    if (topics != null) {
      final v = topics;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['topics'] = arr0;
    }
    data['visibility'] = visibility;
    data['default_branch'] = defaultBranch;
    data['score'] = score;
    return data;
  }
}