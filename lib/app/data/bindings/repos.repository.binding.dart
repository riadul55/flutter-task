import 'package:flutter_task/app/data/repositories/repos.repository.dart';
import 'package:get/get.dart';

import '../services/repos/repos.service.dart';

class ReposRepositoryBinding {
  late ReposRepository _reposRepository;

  ReposRepository get repository => _reposRepository;

  ReposRepositoryBinding() {
    final getConnect = Get.find<GetConnect>();
    final reposService = ReposService(getConnect);
    _reposRepository = ReposRepository(reposService: reposService);
  }
}