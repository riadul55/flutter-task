import 'package:flutter_task/app/data/services/repos/repos.service.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../services/repos/dto/repos.response.dart';
import 'models/ReposModel.dart';

class ReposRepository {
  final ReposService _reposService;
  final _storage = Get.find<GetStorage>();

  ReposRepository({required ReposService reposService})
      : _reposService = reposService;

  Future<ReposModel> getRepos({page = 1, sort = "stars"}) async {
    try {
      final response = await _reposService.getRepositories(page: page, sort: sort);

      final user = ReposModel.fromData(response);
      await user.save();

      return user;
    } catch (e) {
      return ReposModel(data: ReposModel.fromStorage() ?? ReposResponse());
    }
  }
}