import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/repos/dto/repos.response.dart';

class ReposModel {
  final ReposResponse data;

  const ReposModel({
    required this.data,
  });

  factory ReposModel.fromData(ReposResponse response) {
    return ReposModel(data: response);
  }

  static ReposResponse? fromStorage() {
    final storage = Get.find<GetStorage>();
    final repos = storage.read<ReposResponse>('repos');
    return repos;
  }

  Future<void> save() async {
    final storage = Get.find<GetStorage>();
    final repos = storage.read<ReposResponse>('repos');
    var items =  repos?.items ?? [];
    items.addAll(data.items ?? []);
    data.items = items;
    await storage.write('repos', data.toJson());
  }


}