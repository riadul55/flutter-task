import 'package:flutter/material.dart';

class AppColors {
  static var primary = const Color.fromRGBO(25, 51, 117, 1);
  static var onPrimary = Colors.white;

  static var primaryContainer = const Color.fromRGBO(13, 35, 95, 1);
  static var onPrimaryContainer = Colors.white;

  static var secondary = const Color.fromRGBO(25, 51, 117, 1);
  static var onSecondary = Colors.white;

  static var secondaryContainer = const Color.fromRGBO(13, 35, 95, 1);
  static var onSecondaryContainer = Colors.white;

  static var background = const Color.fromRGBO(229, 229, 229, 1);
  static var onBackground = const Color.fromRGBO(74, 77, 85, 1);

  static var error = Colors.red;
  static var onError = Colors.white;

  static var surface = Colors.white;
  static var onSurface = const Color(0xFFE5E5E5);

  static var outline = const Color.fromRGBO(25, 51, 117, 1);

  // text colors
  static var primaryText = const Color.fromRGBO(6, 17, 47, 1);
  static var secondaryText = const Color.fromRGBO(74, 77, 85, 1);

  //status colors
  static var success = const Color.fromRGBO(59, 205, 82, 1);
  static var failed = const Color.fromRGBO(227, 0, 0, 1);
  static var warning = const Color.fromRGBO(255, 193, 35, 1);
}