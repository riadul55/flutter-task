import 'package:flutter/material.dart';
import 'package:flutter_task/app/data/repositories/models/ReposModel.dart';
import 'package:get/get.dart';

import '../../../data/bindings/repos.repository.binding.dart';
import '../../../data/services/repos/dto/repos.response.dart';

class HomeController extends GetxController {
  late ReposRepositoryBinding reposRepositoryBinding;

  var repositoryItems = <ReposResponseItems>[].obs;
  var istFetchLoading = true.obs;
  var isLoadMoreLoading = false.obs;
  int pageCount = 1;

  late ScrollController homeScrollController;

  @override
  void onInit() {
    reposRepositoryBinding = ReposRepositoryBinding();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    fetchRepositories();

    homeScrollController = ScrollController(initialScrollOffset: 5.0);
    homeScrollController.addListener(_scrollListener);
  }

  void fetchRepositories() {
    pageCount == 1 ? istFetchLoading(true) : isLoadMoreLoading(true);
    reposRepositoryBinding.repository.getRepos(page: pageCount).then((ReposModel value) {
      print(value.data.toJson());
      if (value != null && value.data != null && value.data.items != null && value.data.items!.isNotEmpty) {
        var items = value.data.items ?? [];
        repositoryItems.addAll(items);
      }
      pageCount == 1 ? istFetchLoading(false) : isLoadMoreLoading(false);
    });
  }

  _scrollListener() {
    if (homeScrollController.offset >=
        homeScrollController.position.maxScrollExtent &&
        !homeScrollController.position.outOfRange) {
      if (repositoryItems.isNotEmpty) {
        pageCount++;
        fetchRepositories();
      }
    }
  }

  void scrollToTop() {
    homeScrollController.animateTo(0, duration: const Duration(seconds: 3), curve: Curves.linear);
  }

  @override
  void onClose() {}
}
