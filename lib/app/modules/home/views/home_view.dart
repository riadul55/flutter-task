import 'package:flutter/material.dart';
import 'package:flutter_task/app/modules/home/views/components/repository_list.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Repositories'),
        centerTitle: false,
      ),
      body: Obx(() {
        if (controller.istFetchLoading.value) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return RepositoryList();
        }
      }),
      bottomNavigationBar: Obx(() => controller.isLoadMoreLoading.value ? const LinearProgressIndicator() : const SizedBox.shrink()),
    );
  }
}
