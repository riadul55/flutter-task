import 'package:flutter/material.dart';
import 'package:flutter_task/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../../../data/services/repos/dto/repos.response.dart';

class RepositoryList extends StatelessWidget {
  RepositoryList({Key? key}) : super(key: key);

  var homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      controller: homeController.homeScrollController,
      child: Column(
        children: homeController.repositoryItems.value.map((ReposResponseItems item) {
          return ListTile(
            title: Text("${item.fullName}"),
          );
        }).toList(),
      ),
    );
  }
}
